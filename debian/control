Source: golint
Maintainer: Debian Go Packaging Team <pkg-go-maintainers@lists.alioth.debian.org>
Uploaders: Martín Ferrari <tincho@debian.org>,
           Anthony Fok <foka@debian.org>,
Section: devel
Testsuite: autopkgtest-pkg-go
Priority: optional
Build-Depends: debhelper-compat (= 12),
               dh-golang (>= 1.17~),
               golang-any,
               golang-golang-x-tools-dev (>= 1:0.0~git20190125),
Standards-Version: 4.5.0
Vcs-Browser: https://salsa.debian.org/go-team/packages/golint
Vcs-Git: https://salsa.debian.org/go-team/packages/golint.git
Homepage: https://github.com/golang/lint
XS-Go-Import-Path: golang.org/x/lint

Package: golint
Architecture: any
Depends: ${misc:Depends},
         ${shlibs:Depends},
Built-Using: ${misc:Built-Using},
Description: Linter for Go source code
 Golint differs from gofmt. Gofmt reformats Go source code, whereas golint
 prints out style mistakes.
 .
 Golint differs from govet. Govet is concerned with correctness, whereas golint
 is concerned with coding style. Golint is in use at Google, and it seeks to
 match the accepted style of the open source Go project.
